﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.Entities;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Services
{
    public class StudentServices : IStudentServices, ITransient
    {
       private readonly IRepository<Student> _repository;
        public StudentServices(IRepository<Student> repository)
        {
            _repository = repository;
        }
        public async Task<IEnumerable<Student>> GetStudentsAsync(int id)
        {
            return await _repository.Where(x => x.Id == id).ToListAsync();
        }
    }
}
