﻿
using System.Threading.Tasks;
using System.Collections.Generic;
using WebApplication1.Entities;

namespace WebApplication1.Services
{
    public interface IStudentServices
    {
        Task<IEnumerable<Student>> GetStudentsAsync(int id);
    }
}
