﻿using Furion.DatabaseAccessor;

namespace WebApplication1.Entities
{
    public  class Student: IEntity
    {
        public int Id {  get; set; }
        public string Name {  get; set; }
    }
}
