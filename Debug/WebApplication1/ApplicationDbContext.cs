﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1
{
    public class ApplicationDbContext :  AppDbContext<ApplicationDbContext>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
