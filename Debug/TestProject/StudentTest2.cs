
using Furion;
using System.Threading.Tasks;
using WebApplication1.Services;
using Xunit;

namespace TestProject
{
    public class StudentTest1
    {
        private readonly IStudentServices _studentServices;

        public StudentTest1()
        {
            _studentServices = App.GetService<IStudentServices>();
        }

        [Fact]
        public async Task Test1()
        {
            var result = await _studentServices.GetStudentsAsync(1);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task Test2()
        {
            var result = await _studentServices.GetStudentsAsync(1);
            Assert.NotNull(result);
        }
    }
}
