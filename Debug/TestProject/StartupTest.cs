﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.Extensions.DependencyInjection;
using System;
using WebApplication1;
using Xunit.Abstractions;
using Xunit.Sdk;
using Xunit;

[assembly: TestFramework("TestProject.StartupTest", "TestProject")]
namespace TestProject
{
    public class StartupTest : XunitTestFramework
    {
        public StartupTest(IMessageSink messageSink) : base(messageSink)
        {
            var services = Inject.Create();
            services.AddDatabaseAccessor(options =>
            {
                options.AddDbPool<ApplicationDbContext>(DbProvider.InMemoryDatabase, connectionMetadata: Guid.NewGuid().ToString());
            });
            
            services.Build();
        }
    }
}
